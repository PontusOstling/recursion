
public class Rekursion {
	
	// A method that counts up to 10 with 1 at a time
	
		public void rekursion(int count) {
			System.out.println("And " + count);
			if (count >= 10) {
				System.out.println("And it is done!");
			} else {
				rekursion(count + 1);
			}
		}

		public static void main(String[] args) {
			
			// Instantiating the class and call my method
			
			Rekursion r = new Rekursion();
			r.rekursion(1);

		}

	}
